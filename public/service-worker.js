importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');


workbox.precaching.precacheAndRoute([
  'index.html', 'manifest.json', 'source.js', 'style.css', 'icon-256.png', 'icon-512.png'
]);
